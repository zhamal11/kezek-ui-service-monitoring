Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [
            'Очень сложная',
            'Очень сложная',
            'Очень сложная',
            'Очень сложная',
            'Простая',
            'Средняя',
            'Cложная',
            'Очень сложная',
            'Очень сложная'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Количество  услуг'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Нур-Cултан',
        data: [49, 71, 106, 129, 144, 176, 135, 148, 216],
        color: '#F6511D'

    }, {
        name: 'Костанай',
        data: [83, 78, 98, 93, 106, 84, 105, 104, 91],
        color: '#00A6ED'

    }]
});

Highcharts.chart('container2', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Historic World Population by Region'
    },
    subtitle: {
        text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
    },
    xAxis: {
        categories: ['Рабочие проекты/ИТД', 'Опломбировка', 'Пуск газа', 'Врезка', 'Рабочие проекты/ИТД'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ''
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Нур-Cултан',
        data: [17, 31, 35, 23, 2],
        color: '#F6511D'
    }, {
        name: 'Костанай',
        data: [13, 15, 47, 48, 6],
        color: '#00A6ED'
    }]
});